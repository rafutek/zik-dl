import os

import music_tag

from .utils import UNKNOWN, Song, Songs, exec_zikdl, filename_wo_ext


def test_option_album_song(song: Song):
    # Given artist, album and url arguments
    args = [f"--album={song.album}", song.url]
    # When executing the program
    exit_code = exec_zikdl(args)
    # Then it must succeed
    assert exit_code == 0
    # the file must be in the right folder
    filepath = os.path.join(".", UNKNOWN, song.album, song.filename)
    assert os.path.exists(filepath)
    # and tags must be correct
    f = music_tag.load_file(filepath)
    assert str(f["title"]) == filename_wo_ext(song.filename)
    assert str(f["artist"]) == ""
    assert str(f["album"]) == song.album


def test_album_option_songs(songs: Songs):
    # Given artist, album and url arguments
    args = [f"--album={songs.album}", songs.url]
    # When executing the program
    exit_code = exec_zikdl(args)
    # Then it must succeed
    assert exit_code == 0
    # all files must be present and tags correct
    album_dir = os.path.join(".", UNKNOWN, songs.album)
    songs_to_check = os.listdir(album_dir)
    for expected_song in songs.filenames:
        assert expected_song in songs_to_check
        f = music_tag.load_file(os.path.join(album_dir, expected_song))
        assert str(f["title"]) == filename_wo_ext(expected_song)
        assert str(f["artist"]) == ""
        assert str(f["album"]) == songs.album
