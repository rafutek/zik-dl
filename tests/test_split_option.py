import os

import music_tag

from .utils import UNKNOWN, Songs, exec_zikdl, filename_wo_ext


def test_split_option_songs(songs2: Songs):
    # Given artist, album and url arguments
    args = ["--split", songs2.url]
    # When executing the program
    exit_code = exec_zikdl(args)
    # Then it must succeed
    assert exit_code == 0
    # all files must be present and tags correct
    album_dir = os.path.join(".", UNKNOWN, UNKNOWN)
    songs_to_check = os.listdir(album_dir)
    for i, expected_song in enumerate(songs2.filenames):
        assert expected_song in songs_to_check
        f = music_tag.load_file(os.path.join(album_dir, expected_song))
        assert str(f["title"]) == filename_wo_ext(expected_song)
        assert str(f["artist"]) == ""
        assert str(f["album"]) == ""
        assert str(f["tracknumber"]) == str(i + 1)
