import os
import shutil

import pytest

from .utils import TESTING_DIR, MultipleArtistsSong, Song, Songs


@pytest.fixture(autouse=True, scope="session")
def before_and_after_all():
    """
    Automatically goes into testing directory
    before the test session and removes it after.
    """
    os.makedirs(TESTING_DIR, exist_ok=True)
    os.chdir(TESTING_DIR)
    yield
    os.chdir("../")
    shutil.rmtree(TESTING_DIR)


@pytest.fixture
def song():
    return Song(
        url="https://www.youtube.com/watch?v=q8n_C3hLB6o",
        filename="moussa - hembebek.opus",
        artist="Moussa",
        album="premier",
        cover="../tests/resources/moussa-premier.jpg",
    )


@pytest.fixture
def song2():
    return Song(
        url="https://soundcloud.com/killbunk/chanel-bag",
        filename="Chanel Bag Prod. @geehues x @1yeezo.mp3",
        artist="KILLBUNK",
        album="",
    )


@pytest.fixture
def songs():
    filenames = [
        "orage.avi.opus",
        "go va.avi.opus",
        "r5.wav.opus",
        "crisss.aif.opus",
    ]
    return Songs(
        url="https://www.youtube.com/playlist?list=PLFKDEI0tU-D7ODVQmfDKCc-5AtYZi4lC_",
        filenames=filenames,
        album="snippets",
        artist="Moussa",
    )


@pytest.fixture
def songs2():
    filenames = [
        "Premier.opus",
        "Neige.opus",
        "Loues.opus",
        "Magic berber X Merwan.opus",
        "Bleu blue X Merwan.opus",
        "Mauvais sens X Yassine Stein.opus",
        "Go va.opus",
        "Oh.opus",
        "Simple X Claire Laffut.opus",
        "Hembebek.opus",
        "33 6 21 29 67 61.opus",
    ]
    return Songs(
        url="https://www.youtube.com/watch?v=c_ST7ficc3s&t=817s",
        filenames=filenames,
        album="Premier",
        artist="Moussa",
    )


@pytest.fixture
def multiple_artists_song():
    return MultipleArtistsSong(
        url="https://www.youtube.com/watch?v=cFOfcsRvAmc",
        filename="moussa - bleu ciel ft. trop nice (audio officiel).opus",
        album="",
        artists=["Moussa", "Nice"],
    )
