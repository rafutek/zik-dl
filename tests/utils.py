import os.path
from dataclasses import dataclass, field
from typing import List, Optional

from zik_dl import main

TESTING_DIR = "testing"
UNKNOWN = "Unknown"


@dataclass
class Song:
    url: str
    filename: str
    artist: str
    album: str
    cover: Optional[str] = field(default=None)


@dataclass
class Songs:
    url: str
    filenames: List[str]
    artist: str
    album: str


@dataclass
class MultipleArtistsSong:
    url: str
    filename: str
    artists: List[str]
    album: str


def exec_zikdl(args: list) -> int:
    """
    Executes zik-dl main function with given arguments
    and returns exit code.

    `args`: list of arguments to pass to zik-dl
    """
    return main(args)


def filename_wo_ext(filepath):
    """
    Returns the filename without extension.

    Ex: ./foo/fuu/fee.mp3 -> fee
    """
    return os.path.splitext(os.path.basename(filepath))[0]
