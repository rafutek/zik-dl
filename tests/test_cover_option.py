import os

import music_tag

from .utils import UNKNOWN, Song, exec_zikdl


def test_option_cover(song: Song):
    # Given artist, album and url arguments
    args = [f"--cover={song.cover}", song.url]
    # When executing the program
    exit_code = exec_zikdl(args)
    # Then it must succeed
    assert exit_code == 0
    # the file must be in the right folder
    filepath = os.path.join(".", UNKNOWN, UNKNOWN, song.filename)
    assert os.path.exists(filepath)
    # and tags must be correct
    f = music_tag.load_file(filepath)
    with open(song.cover, "rb") as expected_cover_file:
        assert f["artwork"].first.data == expected_cover_file.read()


def test_option_cover_not_found(song: Song):
    # Given artist, album and url arguments
    args = ["--cover=sdsdsds", song.url]
    # When executing the program
    # Then it must throw an exception
    try:
        exec_zikdl(args)
    except Exception:
        return True
    assert False
