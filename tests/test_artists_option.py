import os

import music_tag

from .utils import (
    UNKNOWN,
    MultipleArtistsSong,
    Song,
    Songs,
    exec_zikdl,
    filename_wo_ext,
)


def test_artists_option_song(song: Song):
    # Given artist and url arguments
    args = [f"--artists={song.artist}", song.url]
    # When executing the program
    exit_code = exec_zikdl(args)
    # Then it must succeed
    assert exit_code == 0
    # the file must be in the right folder
    filepath = os.path.join(".", song.artist, UNKNOWN, song.filename)
    assert os.path.exists(filepath)
    # and tags must be correct
    f = music_tag.load_file(filepath)
    assert str(f["title"]) == filename_wo_ext(song.filename)
    assert str(f["artist"]) == song.artist
    assert str(f["album"]) == ""


def test_artists_option_songs(songs: Songs):
    # Given artist, album and url arguments
    args = [f"--artists={songs.artist}", songs.url]
    # When executing the program
    exit_code = exec_zikdl(args)
    # Then it must succeed
    assert exit_code == 0
    # all files must be present and tags correct
    album_dir = os.path.join(".", songs.artist, UNKNOWN)
    songs_to_check = os.listdir(album_dir)
    for expected_song in songs.filenames:
        assert expected_song in songs_to_check
        f = music_tag.load_file(os.path.join(album_dir, expected_song))
        assert str(f["title"]) == filename_wo_ext(expected_song)
        assert str(f["artist"]) == songs.artist
        assert str(f["album"]) == ""


def test_artists_option_song_multiple_artists(
    multiple_artists_song: MultipleArtistsSong,
):
    # Given artists and url arguments
    artists_opt = ",".join(multiple_artists_song.artists)
    args = [f"--artists={artists_opt}", multiple_artists_song.url]
    # When executing the program
    exit_code = exec_zikdl(args)
    # Then it must succeed
    assert exit_code == 0
    # the file must be in the right folder
    artists_dir = " X ".join(multiple_artists_song.artists)
    filepath = os.path.join(".", artists_dir, UNKNOWN, multiple_artists_song.filename)
    assert os.path.exists(filepath)
    # and tags must be correct
    f = music_tag.load_file(filepath)
    assert str(f["title"]) == filename_wo_ext(multiple_artists_song.filename)
    assert f["artist"].values == multiple_artists_song.artists
    assert str(f["album"]) == ""
