import os

import music_tag

from .utils import UNKNOWN, Song, Songs, exec_zikdl, filename_wo_ext


def test_no_url():
    # Given no argument
    args = []
    # When executing the program
    exit_code = exec_zikdl(args)
    # Then it must fail
    assert exit_code != 0


def test_invalid_url():
    # Given an invalid url argument
    args = ["https://ww/6o"]
    # When executing the program
    exit_code = exec_zikdl(args)
    # Then it must fail
    assert exit_code != 0


def test_no_options_song(song: Song):
    # Given a valid url argument
    args = [song.url]
    # When executing the program
    exit_code = exec_zikdl(args)
    # Then it must succeed
    assert exit_code == 0
    # the file must be in the right folder
    filepath = os.path.join(".", UNKNOWN, UNKNOWN, song.filename)
    assert os.path.exists(filepath)
    # and tags must be correct
    f = music_tag.load_file(filepath)
    assert str(f["title"]) == filename_wo_ext(song.filename)
    assert str(f["artist"]) == ""
    assert str(f["album"]) == ""
    assert str(f["tracknumber"]) == ""


def test_no_options_song2(song2: Song):
    # Given a valid url argument
    args = [song2.url]
    # When executing the program
    exit_code = exec_zikdl(args)
    # Then it must succeed
    assert exit_code == 0
    # the file must be in the right folder
    filepath = os.path.join(".", UNKNOWN, UNKNOWN, song2.filename)
    assert os.path.exists(filepath)
    # and tags must be correct
    f = music_tag.load_file(filepath)
    assert str(f["title"]) == filename_wo_ext(song2.filename)
    assert str(f["artist"]) == ""
    assert str(f["album"]) == ""


def test_no_options_songs(songs: Songs):
    # Given url of a playlist
    args = [songs.url]
    # When executing the program
    exit_code = exec_zikdl(args)
    # Then it must succeed
    assert exit_code == 0
    # all files must be present and tags correct
    album_dir = os.path.join(".", UNKNOWN, UNKNOWN)
    songs_to_check = os.listdir(album_dir)
    for expected_song in songs.filenames:
        assert expected_song in songs_to_check
        f = music_tag.load_file(os.path.join(album_dir, expected_song))
        assert str(f["title"]) == filename_wo_ext(expected_song)
        assert str(f["artist"]) == ""
        assert str(f["album"]) == ""
