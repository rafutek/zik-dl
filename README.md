```
usage: zik-dl [-h] [--artists ARTISTS] [--album ALBUM] [--split] url

Linux command-line program to download music from YouTube and other websites.

positional arguments:
  url                URL of the song/album to download

optional arguments:
  -h, --help         show this help message and exit
  --artists ARTISTS  comma-separated artists names like "Louis Armstrong,Ella Fitzgerald"
  --album ALBUM      album name
  --split            split song in multiple songs based on timestamps (youtube video description)
  --cover            cover path like "~/Images/cover1.jpg"
```
