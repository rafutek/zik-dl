#!/bin/bash
[[ -z "$PYPI_USER" || -z "$PYPI_PWD" ]] &&
    echo "PYPI_USER and PYPI_PWD must be set" 1>&2 &&
    exit 1

pip3 install -e ".[dev]"
echo "Building..."
rm -rf dist/
python3 setup.py sdist
python3 setup.py bdist_wheel
echo "Uploading..."
export TWINE_USERNAME=$PYPI_USER
export TWINE_PASSWORD=$PYPI_PWD
twine upload dist/*
